# Kompozicija funkcija umesto mutiranja atributa

class Akter:
    def __init__(self, ime, ind_br_pojav):
        self.ime = ime
        self.ind_br_pojav = ind_br_pojav
        self.sredi_index()

    def sredi_index(self):
        self.ind_br_pojav += 1
        self.ind_br_pojav *= 0.56
