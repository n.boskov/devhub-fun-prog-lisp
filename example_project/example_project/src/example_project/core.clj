(ns example-project.core)

;;;; First-class functions

(defn hello [to who]
  (println (str "Hello " to " " who "!")))

(defn bye [to what-to-do]
  (println (str to ", now is " what-to-do " time!")))

(defn countdown [say]
  (if (> say 0)
    (do
      (println say)
      (recur (dec say)))))


;;;; "... assigning them to variables"
(def hello-devhub
  (partial hello "Devhub"))

(hello-devhub "ninjas")
;;;;

;;;; "... passing functions as arguments"
(defn plan [wellcome exit]
  (hello-devhub "ljudi")
  (countdown 3)
  ;; (Thread/sleep 3000)
  (exit "Ljudi" "party"))

;; ovo je obična lista od 3 funkcije - koja se izvršava - to je cool.
(plan hello-devhub bye)
;;;;

;;;; "... returning them as the values from other functions"
(defn estimate-meetup-quality [beer-per-person]
  (if (< beer-per-person 3)
    (fn [excuse]
      (println "Meetup ništa ne valja! \nAli... ")
      (Thread/sleep 3000)
      (println excuse))
    (fn [_]
      (println "Meetup je super, nema potrebe za izgovorima, opušteno."))))

(let [excuse "Napolju je hladno, bolje nam je ovde."]
  ((estimate-meetup-quality 2) excuse))

(let [excuse "Napolju je hladno, bolje nam je ovde."]
  ;; Hi JavaScript callback!
  ((estimate-meetup-quality 4) excuse))
;;;;

;;;; "... storing them in data structures."
(def hello-devhub-ninjas '(hello-devhub "ninjas"))
(eval hello-devhub-ninjas)
;; data structure -> najobicnija lista - normalna stvar u LISP-u
(type hello-devhub-ninjas)
(first hello-devhub-ninjas)
(second hello-devhub-ninjas)
;;;;

;;;; Pure functions

;;;; Referentially Transparent

;; Matematicke funkcije su referencijalno transparentne - i zato su toliko cool.
(+ 2 2)

(defn impressions [who]
  (str who " kaže: " "pa ovo je super!"))

(impressions "Pera")

(defn unpure-impressions [who]
  (str who " kaže: " (clojure.string/trim (slurp "text/impressions.txt"))))

(unpure-impressions "Pera")
;;;;

;;;; Clojure core data strukture ne dozvoljavaju mutaciju

(def a-lista '(1 2 3 4))
(concat a-lista '(5))
;; a-lista isto

(def a-vec '[1 2 3 4 5])
(conj a-vec 12)
;; a-vec isto

(def a-map {:jedan 1 :dva 2 :tri 3 :cetiri 4 :pet 5})
(conj a-map {:deset 10})
;; a-map isto

(def a-set (set [1 2 3 4 5 7 7 7 6 6]))
(conj a-set 13)
;; a-set isto

;; Rekurzija

;; zanemarimo sada reduce, on je već pokupio svoju slavu u poslednje vreme.
(reduce + (range 10))
(reduce + 10 (range 10))

(defn sum
  ([coll]
   (sum coll 0))
  ([coll acc]
   ;; u acc je trenutni rezultat
   (if (empty? coll)
     acc
     (recur (rest coll) (+ (first coll) acc)))))

(sum (range 10))
(sum (range 10) 10)
;;;;

;;;; Kompozicija funkcija > mutiranje atributa

(def akter {:ime "Jedno" :index 3})
(defn sredi-index [index]
  (* 0.56 (inc index)))
;;;;

;;;; Memoization

(defn devhub-is? [my-mood]
  ;; Neko zahtevno racunanje ili neko cekanje
  (Thread/sleep 5000)
  (if (> my-mood 0.3)
    (str "Devhub is great!")
    (str "Devhub sucks...")))

(def memo-devhub-is? (memoize devhub-is?))
;;;;

;;;; Macro

;; Kod su podaci podaci su kod

(let [infix (read-string "(1 + 1)")]
  (list (second infix) (first infix) (last infix)))
;; vraca kod

(eval
 (let [infix (read-string "(1 + 1)")]
   (list (second infix) (first infix) (last infix))))
;; izvrsiv

;; Ovo je prirodno u Lispu i zato su tu reader makroi

(defmacro example-m
  [code]
  (let [first-p (first code)
        second-p (second code)
        last-p (last code)]
    `(str "procitao sam od " ~first-p " do " ~last-p)))

(example-m (+ 1 2 3 4 (print "Hello World!")))
;; cita kod kao simbole

(defmacro infix
  [infixed]
  (list (second infixed)
        (first infixed)
        (last infixed)))

(infix (1 + 2))

;; DShell - meta

;;;;
