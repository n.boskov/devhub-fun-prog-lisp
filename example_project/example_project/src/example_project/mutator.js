// Mutiranje objekata

var store = {
    kiflice: 12,
    palacinke: 10,
    pivo: 12
};

// nije pure
var evilMutator = function(object){
    object.kiflice -= 2;
};

evilMutator(store);
// store.kiflice


var data = [1, 2, 3, 4, 5];
// sporedan efekat na ovome
var accumulator = [];
var l = data.length;

for(var i=0; i < l; i++) {
    if (data[i] > 2) {
        accumulator.push(data[i]);
    }
}
